#! -*- coding: utf-8 -*-
from django.db import models


class Book(models.Model):

    username = models.CharField(verbose_name=u'Имя', max_length='100')
    text = models.TextField(verbose_name=u'Запись')
    email = models.EmailField(verbose_name=u'Электронная почта',
                              max_length='100', blank=True)
    homepage = models.CharField(verbose_name=u'Домашняя страница',
                                max_length='100', blank=True)
    date = models.DateTimeField(verbose_name=u'Добавлен', auto_now_add=True)
    likes = models.PositiveIntegerField(verbose_name=u'Нравится', default=0,
                                        blank=True)

    def save(self, *args, **kwargs):
        if not self.likes:
            self.likes = 0
        super(Book, self).save(*args, **kwargs)
