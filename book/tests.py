#! -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import resolve

from book.forms import Book, BookForm


class BookAddTestCase(TestCase):

    def test_add_new_resolve_to_AddView(self):
        found = resolve('/add')
        self.assertEqual(found._func_path, 'book.views.AddView')

    def test_can_not_save_unvalidated_note(self):
        form1 = BookForm({
            'username': '',
            'text': '',
            'email': '',
            'homepage': '',
        })
        self.assertRaises(ValueError, form1.save)

        form2 = BookForm({
            'username': 'vasya',
            'text': '',
            'email': '',
            'homepage': '',
        })
        self.assertRaises(ValueError, form2.save)

        form2 = BookForm({
            'username': '',
            'text': u'У вас тут все плохо -_-',
            'email': '',
            'homepage': '',
        })
        self.assertRaises(ValueError, form2.save)

    def test_can_save_validated_note(self):
        form = BookForm({
            'username': u'Василий',
            'text': u'Тут у вас ничего так!',
            'email': 'email@example.com',
            'homepage': 'example.com',
        })
        form.save()
        new_note = Book.objects.first()
        self.assertEqual(new_note.username, u'Василий')
        self.assertEqual(new_note.email, 'email@example.com')

    def test_render_form_to_template(self):
        response = self.client.get('/add')
        self.assertContains(response, u'Имя')
        self.assertContains(response, u'Запись')
        self.assertContains(response, u'Электронная почта')
        self.assertContains(response, u'Домашняя страница')

    def test_validation_username_text_fields_template_errors(self):
        """
        Fields username and text required.
        """
        response = self.client.post(
            '/add',
            data={
                'username': '',
                'text': '',
                'email': '',
                'homepage': '',
            }
        )
        # &quot - " html symbol for sake of security
        self.assertContains(
            response,
            u'Поле &quot;Имя&quot; обязательно для заполнения')
        self.assertContains(
            response, u'Поле &quot;Запись&quot; обязательно для заполнения'
        )

    def test_success_adding_redirects_to_home_page(self):

        response = self.client.post(
            '/add',
            data={
                'username': 'django',
                'text': 'blablablablablablblablablablablablblablablablablabla',
                'email': 'bla@bla.to',
                'homepage': 'github.com',
            }
        )
        self.assertRedirects(response, '/')


class BookListTestCase(TestCase):

    def setUp(self):
        Book.objects.create(
            username=u'Василий',
            text=u'Тут у вас ничего так!',
            email='email@example.com',
            homepage='example.com',
        )
        Book.objects.create(
            username=u'Виталий',
            text=u'Тут у вас ничего так!',
            email='email@example.com',
            homepage='example.com',
        )
        Book.objects.create(
            username=u'Антон',
            text=u'Тут у вас ничего так!',
            email='email@example.com',
            homepage='example.com',
        )
        Book.objects.create(
            username=u'Алексей',
            text=u'Тут у вас ничего так!',
            email='email@example.com',
            homepage='example.com',
            likes=4
        )
        Book.objects.create(
            username=u'Константин',
            text=u'Тутвас ничего так!',
            email='email@example.com',
            homepage='example.com',
        )

    def test_home_resolve_to_Homeview(self):
        found = resolve('/')
        self.assertEqual(found._func_path, 'book.views.HomeView')

    def test_home_page_displays_notes_list_correct_pagination(self):

        self.assertEqual(Book.objects.count(), 5)
        response = self.client.get('/')
        # Pagination: only 4 notes on page
        self.assertContains(response, 'note', count=4)

    def test_home_page_shows_likes(self):
        response = self.client.get('/')
        self.assertContains(response, 'Нравится: 0')
        self.assertContains(response, 'Нравится: 4')

    def test_like_link_add_likes_to_model(self):
        note = Book.objects.first()
        self.assertEqual(note.likes, 0)
        self.client.get('/like/{note_id}'.format(note_id=note.pk))
        note = Book.objects.first()
        self.assertEqual(note.likes, 1)

    def test_sorting_fields_not_contains_id(self):
        response = self.client.get('/')
        self.assertContains(response, u'Имя')
        self.assertContains(response, u'Запись')
        self.assertContains(response, u'Электронная почта')
        self.assertContains(response, u'Домашняя страница')
        self.assertContains(response, u'Добавлен')
        self.assertContains(response, u'Нравится')
        self.assertNotContains(response, 'ID')
