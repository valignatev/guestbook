#! -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView

from sortable_listview import SortableListView

from .models import Book
from .forms import BookForm


class HomeView(SortableListView):
    model = Book
    paginate_by = 4
    allowed_sort_fields = {
        'username': {
            'default_direction': '',
            'verbose_name': model._meta.get_field('username').verbose_name
        },
        'text': {
            'default_direction': '',
            'verbose_name': model._meta.get_field('text').verbose_name
        },
        'email': {
            'default_direction': '',
            'verbose_name': model._meta.get_field('email').verbose_name
        },
        'homepage': {
            'default_direction': '',
            'verbose_name': model._meta.get_field('homepage').verbose_name
        },
        'date': {
            'default_direction': '-',
            'verbose_name': model._meta.get_field('date').verbose_name
        },
        'likes': {
            'default_direction': '',
            'verbose_name': model._meta.get_field('likes').verbose_name
        },
    }

    default_sort_field = 'date'


class AddView(CreateView):
    template_name_suffix = '_add'
    model = Book
    form_class = BookForm
    success_url = '/'


def like(request, note_id):
    # Redirect to home page if note does not exist
    try:
        note = Book.objects.get(pk=note_id)
    except Book.DoesNotExist:
        return HttpResponseRedirect('/')

    note.likes += 1
    note.save()
    return HttpResponseRedirect('/')
