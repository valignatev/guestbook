# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='likes',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041d\u0440\u0430\u0432\u0438\u0442\u0441\u044f'),
        ),
    ]
