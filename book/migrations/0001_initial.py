# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=b'100', verbose_name='\u0418\u043c\u044f')),
                ('text', models.TextField(verbose_name='\u0417\u0430\u043f\u0438\u0441\u044c')),
                ('email', models.EmailField(max_length=b'100', verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430', blank=True)),
                ('homepage', models.CharField(max_length=b'100', verbose_name='\u0414\u043e\u043c\u0430\u0448\u043d\u044f\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430', blank=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d')),
            ],
        ),
    ]
