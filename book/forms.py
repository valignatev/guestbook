#! -*- coding: utf-8 -*-
from django.forms import ModelForm
from .models import Book


class BookForm(ModelForm):

    class Meta:
        model = Book
        exclude = ['date', 'likes']
        error_messages = {
            'username': {
                'required': 'Поле "Имя" обязательно для заполнения',
            },
            'text': {
                'required': 'Поле "Запись" обязательно для заполнения',
            },

        }

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['text'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['email'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['homepage'].widget.attrs.update({
            'class': 'form-control',
        })
